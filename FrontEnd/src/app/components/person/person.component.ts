import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/model/person';
import { PersonService } from 'src/app/services/person.service';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  person = new Person();
  submitted = false;
  msgError = '';
  isDisabled = true; 

  constructor(private personService: PersonService) { }

  ngOnInit(): void {
  }

  savePerson(): void {
    const data = {
      fullname: this.person.fullname,
      birth: this.person.birth,
    };

    this.personService.create(data)
      .subscribe(
        data => {
          this.submitted=true;
          console.log(data);
        },
        err => {
          this.msgError  = err.error.message;
          console.log(err);
        });
  }

  newPerson() {
    this.submitted = false;
    this.person.fullname = null;
    this.person.birth = null;
    this.isDisabled = true;
  }

}
