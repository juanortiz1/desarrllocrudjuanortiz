import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/model/person';
import { PersonService } from 'src/app/services/person.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-list-person',
  templateUrl: './list-person.component.html',
  styleUrls: ['./list-person.component.css']
})
export class ListPersonComponent implements OnInit {

  personSet: Person[];
  personFil: Person[];
  fullnamePerson : string;
  collectionSize: number;
  idPerson: string;
  searchTerm: string;
  closeModal: string;
  msgError = '';
  currentPerson = null;
  currentIndex = -1;
  constructor(private personService: PersonService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.refreshList();
  }  
  
  triggerModal(content:any, val:Person) {
    this.currentPerson = val
    this.retrievePerson(this.currentPerson.id)
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  search(value: string): void {
    this.personFil = this.personSet.filter((val) => val.fullname.toLowerCase().includes(value));
    this.collectionSize = this.personFil.length;
  }

  retrievePersons(): void {
    this.personService.getAll()
      .subscribe(
        data => {
          this.personSet = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  retrievePerson(val:string): void {
    this.personService.get(val)
      .subscribe(
        data => {
          this.currentPerson = data;
          console.log(data);
        },
        error => {
          this.msgError =  error.message +' \n '+ error.error.message;
          console.log(error);
        });
  }

  updatePerson(): void {
   this.personService.update(this.currentPerson.id, this.currentPerson)
      .subscribe(
        data => {
          this.refreshList();
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  deletePerson(val1:string): void {
    this.personService.delete(val1)
       .subscribe(
         data => {
           this.refreshList();
           console.log(data);
         },
         error => {
           console.log(error);
         });
   }

  setActivePerson(person: Person, index : number): void {
    this.currentPerson = person;
    this.currentIndex = index
  }

  refreshList(): void {
    this.retrievePersons();
  }


}
