export class Person {
    id: number;
    fullname: string;
    birth: Date;
}
