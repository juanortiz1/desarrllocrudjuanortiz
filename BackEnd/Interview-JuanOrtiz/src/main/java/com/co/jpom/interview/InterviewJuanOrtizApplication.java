package com.co.jpom.interview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterviewJuanOrtizApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterviewJuanOrtizApplication.class, args);
	}

}
