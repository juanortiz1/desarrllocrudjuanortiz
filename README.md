# README #

En este archivo se describe brevemente el funcionamiento del código y la ejecución esperada por parte del usuario. 


### ¿Qué objetivo tiene este repositorio? ###

En el presente repositorio se aloja el código de la prueba técnica para el puesto de trabajo "Desarrollador FullStack".
Se realiza un crud de persona, el cual cuenta con funciones de:
	* Insertar
	* Actualizar
	* Buscar
	* Eliminar
Un objeto de tipo "Person".

### ¿Cómo se debe ejecutar el Backend? ###

Para el correcto funcionamiento del backend se espera que el usuario descargue el contenido de la carpeta "Backend", una vez descargado deberá abrir el archivo en un IDE que soporte el lenguaje de "Java"
Verificar que todas las dependencias estén instaladas correctamente y ejecutar el programa hasta que se obtenga un mensaje en consola con una información similar a:
 
 "Started InterviewJuanOrtizApplication in 3.32 seconds (JVM running for 3.965)

Una vez seamos notificados con el mensaje anterior el Backend está en su ejecución normal (Estado: En ejecución - Sin errores)

### ¿Cómo se debe ejecutar el Frontend? ###

Para el correcto funcionamiento del frontend el usuario debera descargar el contenido de la carpeta "Frontend", una vez descargados deberá dirigirse a su IDE favorito y en este hacer la instalación correspondientes, 
si la instalación fue éxitosa podremos continuar con el proceso y por último deberemos escribir en consola el comando: "npm start", esto ejecutara nuestra aplicación en angular y nos otorgará el link en el cual se 
encuentra hospedado el servidor local.

Usualmente: http://localhost.4200

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact